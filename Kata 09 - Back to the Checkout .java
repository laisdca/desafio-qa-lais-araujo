﻿class Discount

  def initialize(value, quantity)
    @value = value
    @quantity = quantity
  end

  def calculate_for(quantity)
    (quantity / @quantity).floor * @value
  end

end

class PricePolicy

  def initialize(starts_at, *discounts)
    @starts_at = starts_at
    @discounts = discounts
  end

  def price_for(quantity)
    quantity * @starts_at - discount_for(quantity)
  end

  def discount_for(quantity)
    @discounts.inject(0) do |mem, discount|
      mem + discount.calculate_for(quantity)
    end
  end
end

RULES = {
  'A' => PricePolicy.new(50, Discount.new(20, 3)),
  'B' => PricePolicy.new(30, Discount.new(15, 2)),
  'C' => PricePolicy.new(20),
  'D' => PricePolicy.new(15),
}

class CheckOut

  def initialize(rules)
    @rules = rules
    @items = Hash.new
  end

  def scan(item)
    @items[item] ||= 0
    @items[item] += 1
  end

  def total
    @items.inject(0) do |mem, (item, quantity)|
      mem + price_for(item, quantity)
    end
  end

  private
  def price_for(item, quantity)
    if rule_for(item)
      rule_for(item).price_for(quantity)
    else
      raise "Invalid item '#{item}'"
    end
  end

  def rule_for(item)
    @rules[item]
  end
end


require 'test/unit'
require_relative './checkout.rb'

class TestPrice < Test::Unit::TestCase

  def price(goods)
    co = CheckOut.new(RULES)
    goods.split(//).each { |item| co.scan(item) }
    co.total
  end

  def test_totals
    assert_equal(  0, price(""))
    assert_equal( 50, price("A"))
    assert_equal( 80, price("AB"))
    assert_equal(115, price("CDBA"))

    assert_equal(100, price("AA"))
    assert_equal(130, price("AAA"))
    assert_equal(180, price("AAAA"))
    assert_equal(230, price("AAAAA"))
    assert_equal(260, price("AAAAAA"))

    assert_equal(160, price("AAAB"))
    assert_equal(175, price("AAABB"))
    assert_equal(190, price("AAABBD"))
    assert_equal(190, price("DABABA"))
  end

  def test_incremental
    co = CheckOut.new(RULES)
    assert_equal(  0, co.total)
    co.scan("A");  assert_equal( 50, co.total)
    co.scan("B");  assert_equal( 80, co.total)
    co.scan("A");  assert_equal(130, co.total)
    co.scan("A");  assert_equal(160, co.total)
    co.scan("B");  assert_equal(175, co.total)
  end
end